#!/usr/bin/python3

# pip3 install --pre astroquery
# pip3 install keyrings.alt
from astroquery import open_exoplanet_catalogue as oec
from astroquery.open_exoplanet_catalogue import findvalue
from astroquery.exoplanet_orbit_database import ExoplanetOrbitDatabase
#from astroquery.nasa_exoplanet_archive import NasaExoplanetArchive

from astropy import units as u
from astropy.time import Time

import math

def getSystemName(system):
    return list(system)[0].text

def getIndexOf(l, name):
    for i in range(len(l)):
        if getSystemName(l[i]) == name :
            return i
    return -1


cata=oec.get_catalogue()
xmlsystemslist=list(cata.getroot())
#print(getIndexOf(xmlsystemslist, "Epsilon Indi"))
#print(getIndexOf(xmlsystemslist, "WISE 0855-0714"))
#print(getIndexOf(xmlsystemslist, "PSR B1620-26"))
#print(getIndexOf(xmlsystemslist, "16 Cygni"))

def gettagvalue(s, unit):
    try:
        return float(s.text) * unit
    except:
        try:
            return (float(s.get("upperlimit")) + float(s.get("lowerlimit"))) / 2.0 * unit
        except:
            try:
                return float(s.get("upperlimit")) * unit
            except:
                try:
                    return float(s.get("lowerlimit")) * unit
                except:
                    return None

def get(body, tag, base_unit, to_unit):
    s=body.find(tag)
    if s == None:
        return None
    val=gettagvalue(s, base_unit)
    if val == None:
        return None
    return val.to(to_unit).value

def tojd(body, tag):
    val=get(body, tag, u.m / u.m, '')
    if val == None:
        return None
    return Time(val, format="jd")


def removeNones(dic):
    return {k:v for k,v in dic.items() if v is not None}

def jdToUt(jd):
    if jd == None:
        return None
    return (jd - 2451544.5) * 24 * 3600

def computeMeanAnomalyAtEpoch(xmlorbit):
    period = get(xmlorbit, 'period', u.d, 's')
    pertime = get(xmlorbit, 'periastrontime', u.d / u.d, '')
    if period == None or pertime == None:
        return None
    uT = jdToUt(pertime)
    return 2.0 * math.pi * (uT % period) / period

def getSep(xmlbinary, unit, unitstr):
    xmlsep=xmlbinary.findall('separation')
    separation = None
    for s in xmlsep:
        if s.get("unit") == unitstr:
            separation = gettagvalue(s, unit)
    return separation

def getSMAFromNASA(plt_name):
    sma = 0.0 * u.au
    try:
        #smas = NasaExoplanetArchive.query_object(plt_name, table="exomultpars")['mpl_orbsmax']
        for i in range(len(smas)-1, -1, -1):
            if smas[i].value != 0.0:
                sma = smas[i]
    except:
        pass
    sma = sma.to('m').value
    if sma == 0.0:
        sma = None
    return sma

def getPeriodFromNASA(plt_name):
    period = 0.0 * u.d
    try:
        #periods = NasaExoplanetArchive.query_object(plt_name, table="exomultpars")['mpl_orbper']
        for i in range(len(periods)-1, -1, -1):
            if periods[i].value != 0.0:
                period = periods[i]
    except:
        pass
    period = period.to('s').value
    if period == 0.0:
        period = None
    return period

debug=False
# PARSE ORBIT
def xmlorbittodict(xmlorbit, plt_name = "", starmass=0.0):
    global debug
    result = {
        "ascendingNodeLongitude": get(xmlorbit, 'ascendingnode', u.deg, 'rad'),
        "eccentricity": get(xmlorbit, 'eccentricity', u.m / u.m, ''),
        "inclination": get(xmlorbit, 'inclination', u.deg, 'rad'),
        "meanAnomalyAtEpoch": computeMeanAnomalyAtEpoch(xmlorbit),
        "periapsisArgument": get(xmlorbit, 'periastron', u.deg, 'rad'),
        "semiMajorAxis": get(xmlorbit, 'semimajoraxis', u.au, 'm'),
        "period": get(xmlorbit, 'period', u.d, 's'),
    }
    #if debug and result["semiMajorAxis"] != None:
    #    print("\tSMA:", result["semiMajorAxis"])
    #    print("\tPERIOD:", result["period"])
    try:
        result["separationMeters"] = getSep(xmlorbit, u.AU, 'AU').to('m').value
    except:
        result["separationMeters"] = None
    try:
        result["separationArcsec"] = getSep(xmlorbit, u.arcsec, 'arcsec').value
    except:
        result["separationArcsec"] = None

    if plt_name != "":
        massOK = (starmass != None and starmass > 0.0)
        smaOK = (result["semiMajorAxis"] != None or result["separationMeters"] != None or result["separationArcsec"] != None)
        periodOK = (result["period"] != None)
        if massOK:
            if not smaOK and not periodOK:
                result["semiMajorAxis"] = getSMAFromNASA(plt_name)
                result["period"] = getPeriodFromNASA(plt_name)
        else:
            if not smaOK:
                result["semiMajorAxis"] = getSMAFromNASA(plt_name)
            if not periodOK:
                result["period"] = getPeriodFromNASA(plt_name)

    result["positionAngle"] = get(xmlorbit, 'positionangle', u.deg, 'rad')
    result["transitTime"] = jdToUt(get(xmlorbit, 'transittime', u.d / u.d, ''))
    result["maximumRVTime"] = jdToUt(get(xmlorbit, 'maximumrvtime', u.d / u.d, ''))
    result = removeNones(result)
    if result == {}:
        return None
    return result

# PARSE BINARY
def xmlbinarytodict(xmlbinary):
    result = xmlelementtodict(xmlbinary)
    try:
        result["name"] = xmlbinary.find('name').text
    except:
        pass

    # assign orbit to one child
    obt = xmlorbittodict(xmlbinary)
    if obt != None:
        if "stars" in result.keys():
            if len(result["stars"]) != 0:
                result["stars"][0]["orbit"] = obt
            elif "binaries" in result.keys() and len(result["binaries"]) != 0:
                result["binaries"][0]["orbit"] = obt
        elif "binaries" in result.keys() and len(result["binaries"]) != 0:
            result["binaries"][0]["orbit"] = obt

    result["magB"] = get(xmlbinary, 'magB', u.m / u.m, '')
    result["magV"] = get(xmlbinary, 'magV', u.m / u.m, '')
    result["magR"] = get(xmlbinary, 'magR', u.m / u.m, '')
    result["magI"] = get(xmlbinary, 'magI', u.m / u.m, '')
    result["magJ"] = get(xmlbinary, 'magJ', u.m / u.m, '')
    result["magH"] = get(xmlbinary, 'magH', u.m / u.m, '')
    result["magK"] = get(xmlbinary, 'magK', u.m / u.m, '')
    result = removeNones(result)
    return result

def getStarMassFromNASA(starname):
    mass = 0.0 * u.solMass
    try:
        #masses = NasaExoplanetArchive.query_object(starname, table="exomultpars")['mst_mass']
        for i in range(len(masses)-1, -1, -1):
            if masses[i].value != 0.0:
                mass = masses[i]
    except:
        pass
    mass = mass.to('kg').value
    if mass == 0.0:
        mass = None
    return mass

# PARSE STAR
def xmlstartodict(xmlstar):
    global debug
    if get(xmlstar, 'mass', u.solMass, 'kg') == None:
        debug = True
        #print(xmlstar.find('name').text)
    mass = get(xmlstar, 'mass', u.solMass, 'kg')
    if mass == None:
        mass = getStarMassFromNASA(xmlstar.find('name').text)

    result = xmlelementtodict(xmlstar, mass)
    result["name"] = xmlstar.find('name').text
    result["mass"] = mass
    result["radius"] = get(xmlstar, 'radius', u.solRad, 'm')
    result["temperature"] = get(xmlstar, 'temperature', u.K, 'K')
    result["age"] = get(xmlstar, 'age', u.Gyr, 'Gyr')
    result["metallicity"] = get(xmlstar, 'metallicity', u.m / u.m, '')
    try:
        result["spectraltype"] = xmlstar.find('spectraltype').text
    except:
        pass
    result["magB"] = get(xmlstar, 'magB', u.m / u.m, '')
    result["magV"] = get(xmlstar, 'magV', u.m / u.m, '')
    result["magR"] = get(xmlstar, 'magR', u.m / u.m, '')
    result["magI"] = get(xmlstar, 'magI', u.m / u.m, '')
    result["magJ"] = get(xmlstar, 'magJ', u.m / u.m, '')
    result["magH"] = get(xmlstar, 'magH', u.m / u.m, '')
    result["magK"] = get(xmlstar, 'magK', u.m / u.m, '')
    result = removeNones(result)
    if get(xmlstar, 'mass', u.solMass, 'kg') == None:
        debug = False
    return result

#PARSE PLANET
def xmlplanettodict(xmlplanet,starmass):
    result = xmlelementtodict(xmlplanet)
    result["name"] = xmlplanet.find('name').text
    result["orbit"] = xmlorbittodict(xmlplanet, result["name"], starmass)
    result["impactparameter"] = get(xmlplanet, 'impactparameter', u.m / u.m, '')
    result["mass"] = get(xmlplanet, 'mass', u.Mjup, 'kg')
    result["radius"] = get(xmlplanet, 'radius', u.Rjup, 'm')
    result["temperature"] = get(xmlplanet, 'temperature', u.K, 'K')
    result["age"] = get(xmlplanet, 'age', u.Gyr, 'Gyr')
    result["metallicity"] = get(xmlplanet, 'metallicity', u.m / u.m, '')
    try:
        result["spectraltype"] = xmlplanet.find('spectraltype').text
    except:
        pass
    result["magB"] = get(xmlplanet, 'magB', u.m / u.m, '')
    result["magV"] = get(xmlplanet, 'magV', u.m / u.m, '')
    result["magR"] = get(xmlplanet, 'magR', u.m / u.m, '')
    result["magI"] = get(xmlplanet, 'magI', u.m / u.m, '')
    result["magJ"] = get(xmlplanet, 'magJ', u.m / u.m, '')
    result["magH"] = get(xmlplanet, 'magH', u.m / u.m, '')
    result["magK"] = get(xmlplanet, 'magK', u.m / u.m, '')
    try:
        result["discoverymethod"] = xmlplanet.find('discoverymethod').text
    except:
        pass
    try:
        result["istransiting"] = xmlplanet.find('istransiting').text
    except:
        pass
    try:
        result["description"] = xmlplanet.find('description').text
    except:
        pass
    try:
        result["discoveryyear"] = xmlplanet.find('discoveryyear').text
    except:
        pass
    try:
        result["lastupdate"] = xmlplanet.find('lastupdate').text
    except:
        pass
    result["spinorbitalignment"] = get(xmlplanet, 'spinorbitalignment', u.deg, 'rad')

    result = removeNones(result)
    return result


def xmlbinariestodict(xmlbinaries):
    for i in range(len(xmlbinaries)):
        xmlbinaries[i] = xmlbinarytodict(xmlbinaries[i])
    return xmlbinaries

def xmlstarstodict(xmlstars):
    for i in range(len(xmlstars)):
        xmlstars[i] = xmlstartodict(xmlstars[i])
    return xmlstars

def xmlplanetstodict(xmlplanets,mass):
    for i in range(len(xmlplanets)):
        xmlplanets[i] = xmlplanettodict(xmlplanets[i],mass)
    return xmlplanets


def xmlelementtodict(element, mass=0.0):

    result={}

    if len(element.findall('binary')) > 0:
        xmlbinaries=element.findall('binary')
        result["binaries"] = xmlbinariestodict(xmlbinaries)

    if len(element.findall('star')) > 0:
        xmlstars=element.findall('star')
        result["stars"] = xmlstarstodict(xmlstars)

    if len(element.findall('planet')) > 0:
        xmlplanets=element.findall('planet')
        result["planets"] = xmlplanetstodict(xmlplanets,mass)

    return result

from astropy.coordinates import SkyCoord
def getDEC(xmlsystem):
    decstr=xmlsystem.find('declination').text
    pos1=decstr.find(' ')
    pos2=decstr.find(' ', pos1+1)
    l=list(decstr)
    l[pos1] = '°'
    l[pos2] = '\''
    decstr = ''.join(l)
    return (SkyCoord("0h " + decstr).dec.value * u.deg).to('rad').value

def getRA(xmlsystem):
    rastr=xmlsystem.find('rightascension').text
    pos1=rastr.find(' ')
    pos2=rastr.find(' ', pos1+1)
    l=list(rastr)
    l[pos1] = 'h'
    l[pos2] = 'm'
    rastr = ''.join(l)
    return (SkyCoord(rastr + " 0°").ra.value * u.deg).to('rad').value


xmlsystemslist=list(cata.getroot())
i=0
systems={}
for xmlsystem in xmlsystemslist:
    i += 1
    #PARSE SYSTEM
    sysname = xmlsystem.find('name').text
    systems[sysname] = xmlelementtodict(xmlsystem)
    try:
        systems[sysname]["declination"] = getDEC(xmlsystem)
    except:
        pass
    try:
        systems[sysname]["rightascension"] = getRA(xmlsystem)
    except:
        pass
    systems[sysname]["distance"] = get(xmlsystem, 'distance', u.pc, 'pc')
    if systems[sysname]["distance"] == None:
        try:
            systems[sysname]["distance"] = ExoplanetOrbitDatabase.query_planet(sysname + ' b')['DIST'].value
        except:
            pass
        if systems[sysname]["distance"] == None or systems[sysname]["distance"] == 0.0:
            try:
                pass
                #systems[sysname]["distance"] = NasaExoplanetArchive.query_star(sysname)['st_dist'].value[0]
            except:
                pass

    systems[sysname]["epoch"] = jdToUt(get(xmlsystem, 'epoch', u.d / u.d, ''))
    systems[sysname] = removeNones(systems[sysname])
    print(str(i) + "/" + str(len(xmlsystemslist)))

# WRITE
import os
import json
if not os.path.exists("systems"):
    os.mkdir("systems")

for name in systems.keys():
    try:
        if systems[sysname]["distance"] == None or systems[name]["distance"] == 0.0:
            continue
    except:
        continue
    if not os.path.exists("systems/" + name):
        os.mkdir("systems/" + name)
    with open("systems/" + name + "/definition.json", "w") as f:
        f.write(json.dumps(systems[name], sort_keys=True, indent=4, separators=(',', ': ')))
