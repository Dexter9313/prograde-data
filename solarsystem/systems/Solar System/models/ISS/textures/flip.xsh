#!/usr/bin/env xonsh

for fpath in $(ls *.png).split('\n')[:-1]:
    convert @(fpath) -flip result/@(fpath)

