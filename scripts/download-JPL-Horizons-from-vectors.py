# pip3 install --pre astroquery
# pip3 install keyrings.alt
from astroquery.jplhorizons import Horizons
from astropy import units as u
import os

from poliastro.bodies import Sun, Saturn
from poliastro.bodies import Body
from poliastro.twobody import Orbit

Chury = Body(Sun, 666.2 * u.m * u.m * u.m / (u.s * u.s), "Chury")
query = ['Rosetta', '-226', 'id', '500@1000012', {'start':'2014-05-01', 'stop':'2016-10-05', 'step':'1d'}, Chury]

if not os.path.exists(query[0]):
    os.mkdir(query[0])

obj = Horizons(id=query[1], id_type=query[2], location=query[3], epochs=query[4])
vectors = obj.vectors()

print(vectors['targetname'][0])

vectors['datetime_jd'] -= 2451544.5     # 1 jan 2000 00h00:00.00
vectors['datetime_jd'] *= 24.0 * 3600.0 # to UniversalTime (seconds)
filename = query[0] + "/" + query[0] + "_" + str(int(vectors[0]['datetime_jd'])) + "_" + str(int(vectors[-1]['datetime_jd'])) + '.csv'
vectors['x'].convert_unit_to('m')
vectors['y'].convert_unit_to('m')
vectors['z'].convert_unit_to('m')
vectors['vx'].convert_unit_to('m/s')
vectors['vy'].convert_unit_to('m/s')
vectors['vz'].convert_unit_to('m/s')


table = vectors['datetime_jd', 'x', 'y', 'z', 'vx', 'vy', 'vz']
table.pprint(max_lines=10)

size=len(table)
i=0
with open(filename, 'w') as f:
    f.write("datetime_jd,incl,Omega,w,e,a,M\n")
    for l in table:
        r = [l['x'], l['y'], l['z']] * u.m
        v = [l['vx'], l['vy'], l['vz']] * u.m / u.s
        o=Orbit.from_vectors(query[5], r, v)
        f.write(str(l['datetime_jd']))
        f.write(',' + str(o.inc).split(' ')[0])
        f.write(',' + str(o.argp).split(' ')[0])
        f.write(',' + str(o.raan).split(' ')[0])
        f.write(',' + str(o.ecc))
        f.write(',' + str(o.a.to('m')).split(' ')[0])
        f.write(',' + str(o.M).split(' ')[0])
        f.write("\n")

