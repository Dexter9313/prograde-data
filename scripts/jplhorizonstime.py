from astroquery.jplhorizons import Horizons

def strToJD(strdate):
    if strdate.find(' ') == -1:
        strdate += ' 00:00'
    if len(strdate.split(':')) < 3:
        strdate += ':00'
    if len(strdate.split('.')) > 1:
        strdate = strdate.split('.')[-2][1:]
    obj=Horizons(id='399', id_type='majorbody', epochs={'start':strdate, 'stop':strdate+'.999', 'step':'1m'})
    return float(obj.vectors()[0]['datetime_jd'])

def jdToStr(jd):
    obj=Horizons(id='399', id_type='majorbody', epochs=jd)
    result=obj.vectors()[0]['datetime_str']
    result=result.split('.')[-2]
    result=result[1:]
    return result

def uniformizeStr(strdate):
    return jdToStr(strToJD(strdate))

def addOneDay(strdate):
    return jdToStr(strToJD(strdate) + 1.00001)

def addOneMinute(strdate):
    return jdToStr(strToJD(strdate) + 1.001/(24.0*60.0))
