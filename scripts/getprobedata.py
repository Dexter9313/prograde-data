from astroquery.jplhorizons import Horizons
from astropy import units as u
from astropy.table import vstack, unique
from jplhorizonstime import *
import os
import numpy as np

import threading
import multiprocessing

from poliastro.bodies import Sun, Saturn
from poliastro.bodies import Body
from poliastro.twobody import Orbit


Sun=('Sun', '500@10')
Mercury=('Mercury', '500@199')
Venus=('Venus', '500@299')
Earth=('Earth', '500@399')
Mars=('Mars', '500@499')
Jupiter=('Jupiter', '500@599')
Saturn=('Saturn', '500@699')
Uranus=('Uranus', '500@799')
Neptune=('Neptune', '500@899')
Pluto=('Pluto', '500@999')
Ceres=('Ceres', '500@2000001')
Vesta=('Vesta', '500@2000004')
Chury=('Churyumov-Gerasimenko', '500@1000012', Body(Sun, 666.2 * u.m * u.m * u.m / (u.s * u.s), "Churyumov-Gerasimenko"))

steps={'1d':'30m', '30m':'1m', '1m':'1m'}
stepsValues={'1d':1.0, '30m':30.0/(24.0*60.0), '1m':1.0/(24.0*60.0)}

#INPUT
probes=[
        # for planets, handle B.C. !!!
#        {'name':'Earth', 'id':'399', 'id_type':'majorbody'},
        {'name':'Hubble', 'id':'-48', 'id_type':'id', 'tolerance': (0.03,False), 'patches': [Earth]},
        {'name':'ISS', 'id':'-125544', 'id_type':'id', 'tolerance': (0.03,False), 'patches': [Earth]},
#         {'name':'Gaia', 'id':'-139479', 'id_type':'id', 'tolerance': (0.03,False), 'patches': [Earth]},
        # long to execute !
#        {'name':'Cassini', 'id':'-82', 'id_type':'id', 'patches':
#            [Earth, '1997-10-17', Sun, '1998-04-24', Venus, '1998-05-01', Sun, '1999-06-23',
#                Venus, '1999-06-27', Sun, '1999-08-16', Earth, '1999-08-20', Sun, '2004-05-28',
#                Saturn]
#            },
        # very long to execute !
#        {'name':'Dawn', 'id':'-203', 'id_type':'id', 'tolerance': (0.03,False), 'patches':
#            [Earth, '2007-10-01', Sun, '2009-02-16', Mars, '2009-02-19', Sun, '2011-07-01',
#                Vesta, '2011-08-20',
#                {'loc':Vesta, 'step':'20m'}, '2011-08-21',
#                Vesta, '2011-12-05',
#                {'loc':Vesta, 'step':'10m'}, '2011-12-06',
#                Vesta, '2012-04-07',
#                {'loc':Vesta, 'step':'10m'}, '2012-04-08',
#                Vesta, '2012-09-10', Sun, '2015-02-15',
#                Ceres, '2015-02-27',
#                {'loc':Ceres, 'step':'1d'}, '2015-03-07',
#                Ceres, '2015-06-03',
#                {'loc':Ceres, 'step':'1h'}, '2015-06-04',
#                Ceres, '2015-06-24',
#                {'loc':Ceres, 'step':'1h'}, '2015-07-10',
#                Ceres, '2015-07-17',
#                {'loc':Ceres, 'step':'1h'}, '2015-07-18',
#                Ceres, '2015-08-07',
#                {'loc':Ceres, 'step':'1m'}, '2015-08-07',
#                Ceres, '2015-11-29',
#                {'loc':Ceres, 'step':'1m'}, '2015-11-30',
#                Ceres, '2015-12-31',
#                {'loc':Ceres, 'step':'1m'}, '2016-01-01',
#                Ceres, '2016-01-14',
#                {'loc':Ceres, 'step':'1m'}, '2016-01-15',
#                Ceres, '2016-07-27',
#                {'loc':Ceres, 'step':'1m'}, '2016-07-28',
#                Ceres, '2016-09-04',
#                {'loc':Ceres, 'step':'1m'}, '2016-09-05',
#                Ceres]
#            },
#        {'name':'Juno Spacecraft', 'id':'-61', 'id_type':'id', 'patches':
#            [Earth, '2011-08-11', Sun, '2013-10-08', Earth, '2013-10-12', Sun, '2016-05-27',
#                Jupiter]
#        },
#        {'name':'JWST', 'id':'-170', 'id_type':'id', 'patches':
#            [Earth]
#        },
#        {'name':'Kepler', 'id':'-227', 'id_type':'id', 'patches':
#            [Earth, '2009-03-12', Sun]
#            },
#        {'name':'MAVEN', 'id':'-202', 'id_type':'id', 'patches':
#            [Earth, '2013-11-22', Sun, '2014-09-15', Mars]
#            },
#        {'name':'MESSENGER', 'id':'-236', 'id_type':'id', 'patches':
#            [Earth, '2004-08-05', Sun, '2005-08-01', Earth, '2005-08-06', Sun, '2006-10-23',
#                Venus, '2006-10-26', Sun, '2007-06-04', Venus, '2007-06-08', Sun, '2008-01-14',
#                Mercury, '2008-01-15', Sun, '2008-10-06', Mercury, '2008-10-07', Sun, '2009-09-29',
#                Mercury, '2009-10-01', Sun, '2011-03-16', Mercury]
#            },
#        {'name':'New Horizons', 'id':'-98', 'id_type':'id', 'patches':
#            [Earth, '2006-01-27', Sun, '2007-02-15', Jupiter, '2007-04-13', Sun, '2015-04-01',
#                Pluto, '2015-11-01', Sun]
#            },
#        {'name':'Parker Solar Probe', 'id':'-96', 'id_type':'id', 'patches':
#            [Earth, '2018-08-14', Sun, '2018-10-01', Venus, '2018-10-05', Sun, '2019-12-24',
#                Venus, '2019-12-28', Sun, '2020-07-09', Venus, '2020-07-13', Sun, '2021-02-18',
#                Venus, '2021-02-22', Sun, '2021-10-14', Venus, '2021-10-18', Sun, '2023-08-19',
#                Venus, '2023-08-23', Sun, '2024-11-04', Venus, '2024-11-08', Sun]
#            },
#        {'name':'Pioneer 10', 'id':'-23', 'id_type':'id', 'patches':
#            [Sun, '1973-11-15', Jupiter, '1974-01-02', Sun]
#            },
#        {'name':'Pioneer 11', 'id':'-24', 'id_type':'id', 'patches':
#            [Sun, '1974-11-25', Jupiter, '1975-01-10', Sun, '1979-08-01', Saturn, '1979-10-20', Sun]
#            },
#        {'name':'Rosetta', 'id':'-226', 'id_type':'id', 'patches':
#            [Earth, '2004-03-04', Sun, '2005-03-02', Earth, '2005-03-10', Sun, '2007-02-24',
#                Mars, '2007-02-27', Sun, '2007-11-10', Earth, '2007-11-17', Sun, '2009-11-10',
#                Earth, '2009-11-16', Sun, '2014-05-01',
#                Chury, '2016-05-14',
#                {'loc':Chury, 'step':'1h'}, '2016-05-15',
#                Chury, '2016-05-19',
#                {'loc':Chury, 'step':'1h'}, '2016-05-20',
#                Chury
#                ]},
#        {'name':'Voyager 1', 'id':'-31', 'id_type':'id', 'patches':
#            [Earth, '1977-09-08', Sun, '1979-02-25', Jupiter, '1979-03-24', Sun, '1980-11-05',
#                Saturn, '1980-12-01', Sun]
#            },
#        {'name':'Voyager 2', 'id':'-32', 'id_type':'id', 'patches':
#            [Earth, '1977-08-22', Sun, '1979-06-25', Jupiter, '1979-09-17', Sun, '1981-08-14',
#                Saturn, '1981-09-17', Sun, '1986-01-21', Uranus, '1986-02-03', Sun, '1989-08-20',
#                Neptune, '1989-09-07', Sun]
#            },
]

import time
# func in ["elements", "vectors"]
def safeHorizons(func, raw=False, **kwargs):
    while True:
        try:
            if func == "elements":
                return Horizons(**kwargs).elements(get_raw_response=raw)
            elif func == "vectors":
                return Horizons(**kwargs).vectors(get_raw_response=raw)
        except Exception as ex:
            """
            template = "An exception of type {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            print(message)
            if func == "elements":
                print(Horizons(**kwargs).elements(get_raw_response=True))
            elif func == "vectors":
                print(Horizons(**kwargs).vectors(get_raw_response=True))
            return {}
            """
            pass
        time.sleep(5)

def getElements(probe, patch):
    if len(patch['location']) == 2:
        return safeHorizons("elements", id=probe['id'], id_type=probe['id_type'], location=patch['location'][1], epochs=patch)

    else:
        vectors       = safeHorizons("vectors", id=probe['id'], id_type=probe['id_type'], location=patch['location'][1], epochs=patch)
        vectors['x'].convert_unit_to('m')
        vectors['y'].convert_unit_to('m')
        vectors['z'].convert_unit_to('m')
        vectors['vx'].convert_unit_to('m/s')
        vectors['vy'].convert_unit_to('m/s')
        vectors['vz'].convert_unit_to('m/s')
        table = vectors['datetime_jd', 'datetime_str', 'x', 'y', 'z', 'vx', 'vy', 'vz']

        incl=[]
        Omega=[]
        w=[]
        e=[]
        a=[]
        M=[]
        for i in range(len(table)):
            l=table[i]
            r = [l['x'], l['y'], l['z']] * u.m
            v = [l['vx'], l['vy'], l['vz']] * u.m / u.s
            o=Orbit.from_vectors(patch['location'][2], r, v)
            incl.append(o.inc / u.rad)
            Omega.append(o.raan / u.rad)
            w.append(o.argp / u.rad)
            e.append(o.ecc)
            a.append(o.a.to('m') / u.m)
            M.append(o.M / u.rad)
        table['incl'] = np.array(incl) * u.rad
        table['Omega'] = np.array(Omega) * u.rad
        table['w'] = np.array(w) * u.rad
        table['e'] = e
        table['a'] = np.array(a) * u.m
        table['M'] = np.array(M) * u.rad
        return table

def getFullTrajectoryInterval(probe):
    # make a random request to JPL Horizons to get an answer containing the begin date
    # choose JD=-2000000.0 (~= -10000 BC) to be sure to miss
    resp=safeHorizons("elements", True, id=probe['id'], id_type=probe['id_type'], epochs={'start':'JD-2000001.0', 'stop':'JD-2000000.0', 'step':'1d'})
    # find lines for trajectories (dirty...)
    reading=False
    l=""
    for line in resp.split('\n'):
        if line.find("No ephemeris for target") != -1:
            l=line
    # get date only
    l=l.split('prior to ')[-1]
    l=l.split('-')
    # remove A.D./B.C.
    l[0]=l[0].split(' ')[-1]
    # remove seconds and everything after
    l[-1]=':'.join(l[-1].split(':')[0:2])
    beg = '-'.join(l)
    beg = addOneMinute(beg)

    # make a random request to JPL Horizons to get an answer containing the end date
    # choose JD=3650000.0 (~= 10000 AD) to be sure to miss
    resp=safeHorizons("elements", True, id=probe['id'], id_type=probe['id_type'], epochs={'start':'JD6000000.0', 'stop':'JD6000001.0', 'step':'1d'})
    # find lines for trajectories (dirty...)
    reading=False
    l=""
    for line in resp.split('\n'):
        if line.find("No ephemeris for target") != -1:
            l=line
    # get date only
    l=l.split('after ')[-1]
    l=l.split('-')
    # remove A.D./B.C.
    l[0]=l[0].split(' ')[-1]
    # remove seconds and everything after
    l[-1]=':'.join(l[-1].split(':')[0:2])
    end = uniformizeStr('-'.join(l))

    return {'start':beg, 'stop':end}

def initializePatches(probe):
    interval=getFullTrajectoryInterval(probe)
    patches=[]
    firstMidnight=uniformizeStr(addOneDay(interval['start']).split(' ')[0])

    loc = probe['patches'][0]
    if type(loc) is dict:
        loc = loc['loc']
    patches.append({'start':interval['start'], 'stop':firstMidnight, 'step':'1m', 'location':loc})

    currentStart=firstMidnight
    # proper patches
    for i in range(len(probe['patches']) // 2):
        currentStop=uniformizeStr(probe['patches'][2*i+1])
        loc = probe['patches'][2*i]
        if type(loc) is dict:
            step = loc['step']
            loc = loc['loc']
            custom = True
        else:
            step = '1d'
            custom = False
        patches.append({'start':currentStart, 'stop':currentStop, 'step':step, 'location':loc, 'custom':custom})
        currentStart=currentStop

    lastMidnight=uniformizeStr(interval['stop'].split(' ')[0])
    loc = probe['patches'][-1]
    if type(loc) is dict:
        step = loc['step']
        loc = loc['loc']
        custom = True
    else:
        step = '1d'
        custom = False
    # last proper patch
    patches.append({'start':currentStart, 'stop':lastMidnight, 'step':step, 'location':loc, 'custom':custom})
    # last day
    patches.append({'start':lastMidnight, 'stop':interval['stop'], 'step':'1m', 'location':loc})

    return patches

def patchIsTooBig(start, stop, step):
    return (strToJD(stop) - strToJD(start)) / stepsValues[step] > 11000

# dirty but works and fast
def fastCleanUpStr(strdate):
    return strdate[5:-5]

def checkPatch(probe, patch):
    elem      = getElements(probe, patch)
    x0        = elem['a']
    x1        = elem['e']
    x2        = elem['incl']
    datetimes = elem['datetime_str']

    dx=[]
    registeringEvent=False
    events=[]
    tolerance=0.03
    useIncEcc=True
    if 'tolerance' in probe.keys():
        tolerance = probe['tolerance'][0]
        useIncEcc = probe['tolerance'][1]

    for i in range(len(x0) - 1):
        dxi = [abs((x0[i+1] - x0[i]) / x0[i+1]), abs((x1[i+1] - x1[i]) / x1[i+1]), abs((x2[i+1] - x2[i]) / x2[i+1])]
        if useIncEcc:
            dx.append(max(dxi))
        else:
            dx.append(dxi[0])
        if not registeringEvent and abs(dx[-1]) > tolerance:
                registeringEvent = True
                events.append(fastCleanUpStr(datetimes[i]))
        elif registeringEvent and abs(dx[-1]) < tolerance:
                registeringEvent = False
                events.append(fastCleanUpStr(datetimes[i]))

    if registeringEvent:
        events.append(fastCleanUpStr(datetimes[len(x0) - 1]))

    return events

def subdividePatch(probe, patch):
    # if custom patch, don't touch it
    if 'custom' in patch.keys() and patch['custom']:
        return [patch]
    # if patch is too big, first split it in two
    if patchIsTooBig(patch['start'], patch['stop'], patch['step']):
        midDate = jdToStr((strToJD(patch['start']) + strToJD(patch['stop']))/2.0)
        firstPatch={'start':patch['start'], 'stop':midDate, 'step':patch['step'], 'location':patch['location']}
        secondPatch={'start':midDate, 'stop':patch['stop'], 'step':patch['step'], 'location':patch['location']}
        result1 = subdividePatch(probe, firstPatch)
        result2 = subdividePatch(probe, secondPatch)
        return result1 + result2
    # if already at smallest step, don't divide further
    if patch['step'] == steps[patch['step']]:
        return [patch]
    events = checkPatch(probe, patch)
    # if no event, don't do anything
    if len(events) == 0:
        return [patch]
    print("Step : " + patch['step'])
    print(events)

    result=[]
    step = patch['step']
    currentIsBad=False
    currentDateTime=patch['start']
    for event in events:
        if currentIsBad:
            subPatches = subdividePatch(probe, {'start':currentDateTime, 'stop':event, 'step':steps[step], 'location':patch['location']})
            for subPatch in subPatches:
                result.append(subPatch)
        else:
            result.append({'start':currentDateTime, 'stop':event, 'step':step, 'location':patch['location']})
        currentDateTime = event
        currentIsBad = not currentIsBad
    result.append({'start':currentDateTime, 'stop':patch['stop'], 'step':step, 'location':patch['location']})
    return result

def generateFullPatches(probe):
    initPatches=initializePatches(probe)
    result=[]
    for patch in initPatches:
        for subpatch in subdividePatch(probe, patch):
            # ignore 0-time patches
            if strToJD(subpatch['start']) == strToJD(subpatch['stop']):
                continue
            # if result is empty, just append
            if len(result) == 0:
                result.append(subpatch)
            # merge patches if same location and step and not too big result
            if subpatch['step'] == result[-1]['step'] and subpatch['location'] == result[-1]['location'] and not patchIsTooBig(result[-1]['start'], subpatch['stop'], subpatch['step']):
                result[-1]['stop'] = subpatch['stop']
                continue
            result.append(subpatch)
    return result

def printTables(probename, locationname, tables):
    filename = probename + "/" + probename + "_" + str(int(tables[0][0]['datetime_jd'])) + "_" + str(int(tables[-1][-1]['datetime_jd'])) + '.csv'
    unique(vstack(tables)).write(filename, format='ascii.csv', overwrite=True)

    # write patch
    contents = []
    with open(filename, 'r') as f:
        contents = f.readlines()
    contents.insert(1, '#' + locationname + '\n')
    with open(filename, 'w') as f:
        contents = f.writelines(contents)

numThreads=multiprocessing.cpu_count()

class TableWriter (threading.Thread):
    def __init__(self, probe, patches):
        threading.Thread.__init__(self)
        self.probe = probe
        self.patches = patches
    def run(self):
        tables=[]
        tablesLocationName=""
        for patch in self.patches:
            print(patch)
            elem                 = getElements(self.probe, patch)
            elem['datetime_jd'] -= 2451544.5;    # 1 jan 2000 00h00:00.00
            elem['datetime_jd'] *= 24.0 * 3600.0 # to UniversalTime (seconds)
            elem['incl'].convert_unit_to('rad')
            elem['Omega'].convert_unit_to('rad')
            elem['w'].convert_unit_to('rad')
            elem['a'].convert_unit_to('m')
            elem['M'].convert_unit_to('rad')

            table = elem['datetime_jd', 'incl', 'Omega', 'w', 'e', 'a', 'M']

            if len(tables) > 0 and patch['location'][0] != tablesLocationName:
                printTables(self.probe['name'], tablesLocationName, tables)
                tables=[]
                tablesLocationName=""

            tables.append(table)
            if tablesLocationName == "":
                tablesLocationName = patch['location'][0]

            allEntries=sum([len(t) for t in tables])
            # check patch location too !
            if allEntries > 15000:
                printTables(self.probe['name'], tablesLocationName, tables)
                tables=[]
                tablesLocationName=""
        if len(tables) > 0:
            printTables(self.probe['name'], tablesLocationName, tables)


for probe in probes:
    print(probe['name'])
    if not os.path.exists(probe['name']):
        os.mkdir(probe['name'])

    patches=generateFullPatches(probe)
    print("TOTAL NUMBER OF PATCHES TO WRITE:")
    print(len(patches))
    print("WRITING DATA USING " + str(numThreads) + " THREADS")
    threads=[]
    for i in range(numThreads):
        beg = int(i*len(patches)//numThreads)
        if i < numThreads-1:
            end=int((i+1)*len(patches)//numThreads)
            thread = TableWriter(probe, patches[beg:end])
        else:
            thread = TableWriter(probe, patches[beg:])
        thread.start()
        threads.append(thread)

    for t in threads:
        t.join()
print("COMPUTATION OVER")
