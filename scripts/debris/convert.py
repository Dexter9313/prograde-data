#!/usr/bin/env python3

import json
from datetime import datetime
from math import radians, pi, floor

def mod(a, b):
   q=floor(a/b)
   return a-(b*q)

def SMAfromP(P):
    mu = 6.67408e-11 * 5.97237e+24 # G * Mearth
    return (mu * (P / (2.0*pi))**2)**(1.0/3.0)

with open("./TLE.json", "r") as f:
    s=''.join(f.readlines())

js=json.loads(s)

result=[]

i=0
tot=-1
for j in js:
    name=j['ON']
    ut_year=int(j['TLE1'][18:20])
    if ut_year > 45: # previous century
        ut_year -= 100
    ut_day=float(j['TLE1'][20:32])
    ut = (datetime(2000+ut_year, 1, 1) - datetime(2000, 1, 1)).total_seconds() + 24*3600*ut_day

    inclination=radians(float(j['TLE2'][8:16]))
    LAN=radians(float(j['TLE2'][17:25]))
    ecc=float('0.' + j['TLE2'][26:33])
    pARG=radians(float(j['TLE2'][34:42]))
    M=radians(float(j['TLE2'][43:51]))
    P=24*3600/float(j['TLE2'][52:63])
    SMA=SMAfromP(P)
    ut=mod(ut, P)
    M -= 2*pi * ut/P
    M = mod(M, 2*pi)

    result.append(
            {
                'name':name,
                'orbit': {
                    'LAN': LAN,
                    'ECC':ecc,
                    'INC':inclination,
                    'M':M,
                    'pARG':pARG,
                    'SMA':SMA
                    },
            }
            )
    i+=1
    if i == tot:
        break

print(json.dumps({"data":result}))
