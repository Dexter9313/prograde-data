#!/usr/bin/env python3

import requests
import json
from datetime import datetime
from math import radians, pi, floor, sqrt

def mod(a, b):
   q=floor(a/b)
   return a-(b*q)

def PfromSMA(SMA):
    mu = 6.67408e-11 * 1.98847e+30 # G * Msun
    return 2.0*pi * sqrt(SMA*SMA*SMA/mu);

url = 'https://ssd-api.jpl.nasa.gov/sbdb_query.api?fields=a,i,om,w,e,ma,epoch.cal'
r = requests.get(url, allow_redirects=True)
with open('asteroids.json', 'wb') as f:
    f.write(r.content)

with open("./asteroids.json", "r") as f:
    s=''.join(f.readlines())

js=json.loads(s)["data"]


maxperfile=600000
fileN=0
while fileN*maxperfile < len(js):
    result=[]

    fileN += 1
    for i in range((fileN-1)*maxperfile, min(fileN*maxperfile, len(js))):
        j = js[i]
        try:
            name=str(i)
            SMA=1.496e+11 * float(j[0])# AU to meters
            inclination=radians(float(j[1]))
            LAN=radians(float(j[2]))
            pARG=radians(float(j[3]))
            ecc=float(j[4])
            M=radians(float(j[5]))
            ut_year=j[6].split('-')[0]
            ut_month=j[6].split('-')[1]
            ut_day=j[6].split('-')[2].split('.')[0]
            ut_rest='0.'+j[6].split('.')[1]
            ut = (datetime(int(ut_year), int(ut_month), int(ut_day)) - datetime(2000, 1, 1)).total_seconds() + 24*3600*float(ut_rest)
            P = PfromSMA(SMA)
            ut=mod(ut, P)
            M -= 2*pi * ut/P
            M = mod(M, 2*pi)

            result.append(
                    {
                        #'name':name,
                        'orbit': {
                            'LAN': float("{:.3f}".format(LAN)),
                            'ECC':ecc,
                            'INC':float("{:.3f}".format(inclination)),
                            'M':float("{:.3f}".format(M)),
                            'pARG':pARG,
                            'SMA':SMA
                            },
                    }
                    )
        except:
            pass

    suffix = ""
    if len(js) > maxperfile:
        suffix = "." + str(fileN)

    with open("./Sun" + suffix, 'w') as f:
        f.write(json.dumps({"data":result}))
